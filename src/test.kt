import funtional.Utils
import funtional.Utils.Option
import funtional.Utils.Option.Some
import funtional.Utils.Option.None
import funtional.Utils.Either
import funtional.Utils.Either.Right
import funtional.Utils.Either.Left

/**
 * Created by slorenzot on 12/1/2017.
 */

fun main(args: Array<String>) {

//    val option1 = Option.of(100)
//    println(option1)
//    println(option1.get())
//
//    val option3 = Option.of(null)
//    println(option3)
//    println(option3.getOrElse("Falló"))
//
//    val option4 = Option.empty()
//    println(option4)
//    println(option4.getOrElse("Falló"))
//
//    val option = Option { 2000 / 20 }
//    when (option) {
//        is Some -> println("OK! -> ${option.get()}")
//        is None -> println("FAILED!")
//    }

//    val username = ""
//    val either = Either("Execution has failed with Error", { 100 / 0 })
//
//    when (either) {
//        is Left -> println(either.get())
//        is Right -> println(either.get())
//    }
//
//    val either2 = Either.of("String", {})
//    println(either2.fold())

    val result =
            Either.of("Fail A", { 100 / 0 })
                    .fold()
    when(result) {
        is Right -> "OK!"
        is Left -> println(result.get())
    }

//
//    println(test(true))
//    println(test(false))
//
//    val try1 = Utils.Try { 100 / 0}
//    when(try1) {
//        is Success -> println(try1.value)
//        is Failure -> println(try1.map { it.toString().toUpperCase() })
//    }
//
//    val try2 = Utils.Try { null }
//    when(try2) {
//        is Success -> println(try2.option().getOrElse(1000))
//        is Failure -> println(try2)
//    }

}