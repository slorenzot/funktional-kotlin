package funtional

/**
 * Created by slorenzot on 2/28/2018.
 */
class Utils {

    inline infix fun <T> Boolean.then(param: () -> T) = if (this) param() else null

    sealed class Option<out T>(open val value: T?) {

        companion object {
            inline operator fun <R> invoke(f: () -> R)/*: Option<T>*/ = if (f() != null) Some(f()) else None<R>()

            inline fun <R> of(o: R) = if (o != null) Some(o) else None<R>()

            fun empty() = of(null)
        }

        open fun get(): T? = value
        fun <R> getOrElse(other: R) = value ?: other
        fun <Q> map(g: (v: T) -> Q): Q = g(value!!)
        fun <Q> flatMap(g: (v: T) -> Q): Option<Q> = of(g(value!!))


        open fun isEmpty() = (value == null)
        open fun notEmpty() = !isEmpty()

        class Some<out S>(override val value: S) : Option<S>(value)

        class None<out S> : Option<S>(null) {

            override fun get() = throw Exception("None has no value")

        }

    }

    sealed class Try<out T>(open val value: T) {

        companion object {
            /**
             * Aplica Try y retorna Success o Failed
             */
            inline operator fun <R> invoke(f: () -> R) =
                    try {
                        Success(f())
                    } catch (e: Exception) {
                        Failure(e)
                    }
        }

        fun <Q> map(f: (v: T) -> Q): Q = f(value)

        class Success<out R>(override val value: R) : Try<R>(value) {
            fun option(): Option<R> = Option.of(value)
        }

        class Failure<out E>(private val ex: E) : Try<E>(ex) {
            fun get(): E = value
        }

    }

    open class Either<L, R> {

        var left: L? = null
        var right: () -> R? = { null }

        private constructor()

        private constructor(left: L, right: () -> R) {
            this.left = left
            this.right = right
        }

        companion object {
            /**
             * Aplica Either y retorna Left o Right
             */
            operator fun <L, R> invoke(a: L, b: () -> R) = when (Try({ b() })) {
                is Try.Success -> Either.Right(b())
                is Try.Failure -> Either.Left(a)
            }

            /**
             * Retorna una instancia de Either
             */
            fun <L, R> of(a: L, b: () -> R) = Either(a, b)
        }

        inline fun fold() = when (Try({ right() })) {
            is Try.Success -> Either.Right(right())
            is Try.Failure -> Either.Left(left)
        }

//        fun <S, T> then(after: Either<S, T>) =
//                when (this.fold()) {
//                    is Try.Success<*> -> after.fold()
//                    is Try.Failure<*> -> Left(after.left)
//                    else -> Left(after.left)
//                }

        class Left<L>(private val value: L) : Either<L, Nothing>() {

            companion object {
                inline operator fun <R> invoke(value: R) = Left(value)
            }

            fun <Q> map(g: (v: L) -> Q): Q = g(value!!)
//        override fun <Q> flatMap(g: (v: L) -> Q): Option<Q> = Option(g(value!!))

            fun get() = value
        }

        class Right<R>(private val value: R) : Either<Nothing, R>() {

            companion object {
                inline operator fun <R> invoke(value: R) = Right(value)
            }

            fun <Q> map(g: (v: R) -> Q): Q = g(value!!)
//            fun <Q> flatMap(g: (v: R) -> Q): Option<Q> = Option.of(g(value!!))

            fun get() = value
        }


    }

}